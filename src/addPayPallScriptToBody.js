const clientId = "ATgIfj8ixNJFcTXPPLm3Fiv5dOb-ulftun7t6OMnT1qGVlyUuI96Jp0uZC1-kmFo2o8tQb0u6YOXkSao";

function addPayPallScriptToBody() {
    return new Promise(resolve=>{
        const script = document.createElement('script');
        script.src = `https://www.paypal.com/sdk/js?client-id=${clientId}&currency=USD`;
        document.body.appendChild(script);
        script.addEventListener('load', () => resolve());
        document.body.appendChild(script);
    })
  }

  export default addPayPallScriptToBody;